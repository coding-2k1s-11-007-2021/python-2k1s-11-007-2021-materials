import socketserver


class MyTCPHandler(socketserver.StreamRequestHandler):
    def handle(self):
        data = self.rfile.readline().strip()
        while data:
            print(data)
            data = self.rfile.readline().strip()
        some_content = 'Привет мир!'.encode('utf-8')
        self.wfile.writelines([
            b'HTTP/1.1 200 OK\n',
            b'server: nginx\n',
            b'content-type: text/html;charset=utf-8\n',
            b'last-modified: Sun, 07 Nov 2021 06:10:31 GMT\n',
            b'etag: "61876dd7-161e7"\n',
            b'x-clacks-overhead: GNU Terry Pratchett\n',
            b'strict-transport-security: max-age=315360000; includeSubDomains; preload\n',
            # b'content-encoding: gzip\n',
            b'via: 1.1 varnish, 1.1 varnish\n',
            b'accept-ranges: bytes\n',
            b'date: Sun, 07 Nov 2021 23:32:08 GMT\n',
            b'age: 62358\n',
            b'x-served-by: cache-lga21954-LGA, cache-bma1625-BMA\n',
            b'x-cache: HIT, HIT\n',
            b'x-cache-hits: 2, 1\n',
            b'x-timer: S1636327929.740744,VS0,VE1\n',
            b'vary: Accept-Encoding\n',
            b'content-length: ' + str(len(some_content)).encode('ascii') + b'\n',
            b'X-Firefox-Spdy: h2\n',
            b'\n',
            some_content + b'\n'
        ])


if __name__ == "__main__":
    HOST, PORT = "localhost", 9993

    with socketserver.ThreadingTCPServer((HOST, PORT), MyTCPHandler) as server:
        server.serve_forever()
