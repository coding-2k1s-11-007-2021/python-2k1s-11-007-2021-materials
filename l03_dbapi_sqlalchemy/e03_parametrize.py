import sqlite3


con = sqlite3.connect(':memory:')
try:
    cur1 = con.cursor()

    cur1.execute("create table lang (name, first_appeared)")
    lang_list = [
        ("Fortran", 1957),
        ("Python", 1991),
        ("Go", 2009),
    ]
    cur1.executemany("insert into lang values (?, ?)", lang_list)
    con.commit()

    cur2 = con.cursor()
    cur2.execute("select * from lang where first_appeared=:year", {"year": 1957})
    print(cur2.fetchall())
finally:
    con.close()
