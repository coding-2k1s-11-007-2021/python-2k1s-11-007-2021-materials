import sqlalchemy
from sqlalchemy import text


engine = sqlalchemy.create_engine('sqlite+pysqlite:///:memory:', echo=True, future=True)
with engine.connect() as conn:
    conn.execute(text("CREATE TABLE IF NOT EXISTS some_table (x int, y int)"))
    conn.execute(
        text("INSERT INTO some_table (x, y) VALUES (:x, :y)"),
        [{"x": 1, "y": 1}, {"x": 2, "y": 4}]
    )
    conn.commit()
    result = conn.execute(text("SELECT x, y FROM some_table WHERE y > :y"), {"y": 2})
    for row in result:
        print(f"x: {row.x}  y: {row.y}")
