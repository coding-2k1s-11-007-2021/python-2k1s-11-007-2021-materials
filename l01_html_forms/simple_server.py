from http.server import BaseHTTPRequestHandler, HTTPServer
import logging


class CustomHTTPHandler(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS')
        self.end_headers()

    def _print_request(self):
        print(f'{self.command} {self.path} {self.protocol_version}')
        for header_name, header_value in self.headers.items():
            print(f'{header_name}: {header_value}')
        print()
        if content_length_str := self.headers['Content-Length']:
            print(self.rfile.read(int(content_length_str)))

    def do_GET(self):
        self._print_request()
        self._set_response()
        self.wfile.write(b'Response from GET request')

    def do_POST(self):
        self._print_request()
        self._set_response()
        self.wfile.write(b'Response from POST request')

    def do_PUT(self):
        self._print_request()
        self._set_response()
        self.wfile.write(b'Response from PUT request')

    def do_OPTIONS(self):
        self._print_request()
        self._set_response()
        self.wfile.write(b'Response from OPTIONS request')


def run(server_class=HTTPServer, handler_class=CustomHTTPHandler, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
