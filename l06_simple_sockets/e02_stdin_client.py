import socket

HOST = '127.0.0.1'
PORT = 8090

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    while True:
        s.sendall(input().encode(encoding='utf-8'))
        print(s.recv(1024).decode(encoding='utf-8'))
