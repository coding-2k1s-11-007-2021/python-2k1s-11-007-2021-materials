from flask_login import UserMixin

from database import db


class User(UserMixin, db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    hashed_password = db.Column(db.String(80), unique=False, nullable=False)

    def __repr__(self):
        return f"<User {self.username}>"
