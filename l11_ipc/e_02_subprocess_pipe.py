import subprocess
from glob import glob


def main():
    p1 = subprocess.Popen(['du', '-s'] + glob('/var/*'), stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    p2 = subprocess.Popen(['sort', '-n'], stdin=p1.stdout, stdout=subprocess.PIPE)
    p3 = subprocess.Popen(['tail', '-n', '10'], stdin=p2.stdout, stdout=subprocess.PIPE)
    out, err = p3.communicate()
    for line in out.split(b'\n'):
        print(line.decode('utf-8'))
    print(err)


if __name__ == '__main__':
    main()
