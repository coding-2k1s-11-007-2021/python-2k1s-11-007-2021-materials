import socket


with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
    s.connect('/tmp/unix_socket_example')
    while True:
        s.sendall(input().encode(encoding='utf-8'))
        print(s.recv(1024).decode(encoding='utf-8'))
