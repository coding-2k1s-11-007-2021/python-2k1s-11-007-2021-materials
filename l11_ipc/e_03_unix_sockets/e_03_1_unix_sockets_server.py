import socket


def main():
    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
        s.bind('/tmp/unix_socket_example')
        s.listen(2)
        while True:
            conn, addr = s.accept()
            with conn:
                print('Connected by', addr)
                while True:
                    data = conn.recv(1024)
                    if not data:
                        break
                    conn.sendall(data)


if __name__ == '__main__':
    main()
