from flask import Blueprint, render_template, request, redirect, url_for, flash, jsonify

from company_management.forms import DepartmentForm, EmployeeForm
from company_management.services import get_paginated_departments_list, create_department, DatabaseError, \
    get_department_by_name_part, create_employee

company_management_blueprint = Blueprint('company_management', __name__, template_folder='templates')


@company_management_blueprint.route('/', methods=('GET',))
def main_page():
    return render_template('company_management/main_page.html')


@company_management_blueprint.route('/departments', methods=('GET',))
def departments_list():
    raw_page = request.args.get('page', '1')
    page = int(raw_page) if raw_page.isdigit() else 1
    departments_pagination = get_paginated_departments_list(page)
    return render_template('company_management/departments_list.html', departments_pagination=departments_pagination)


@company_management_blueprint.route('/departments/create', methods=('GET', 'POST'))
def create_department_page():
    form = DepartmentForm()
    if form.validate_on_submit():
        try:
            create_department(form)
        except DatabaseError as e:
            flash(str(e))
        else:
            flash('Successfully created new department')
            return redirect(url_for('company_management.departments_list'))
        return redirect(url_for('company_management.create_department_page'))
    return render_template('company_management/create_department.html', form=form)


@company_management_blueprint.route('/employees/search', methods=('POST',))
def search_department_by_name():
    json_content = request.get_json()
    result = get_department_by_name_part(json_content['name'])
    return jsonify([{'id': d.id, 'name': d.name} for d in result])


@company_management_blueprint.route('/employees/create', methods=('GET', 'POST'))
def create_employee_page():
    form = EmployeeForm()
    if form.validate_on_submit():
        try:
            create_employee(form)
        except DatabaseError as e:
            flash(str(e))
        else:
            flash('Successfully created new employee')
            return redirect(url_for('company_management.main_page'))
        return render_template('company_management/create_employee.html', form=form)
    return render_template('company_management/create_employee.html', form=form)
