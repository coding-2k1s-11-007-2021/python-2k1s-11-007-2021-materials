import pytest
from flask import url_for
from app import db
from company_management.models import Department


def test_department_list_no_items_should_return_no_items(client):
    res = client.get(url_for('company_management.departments_list'))
    assert res.status_code == 200
    assert b'No departments' in res.data


def create_departments(items_count: int):
    for i in range(items_count):
        db.session.add(Department(name=f'Dep {i}'))
    db.session.commit()


@pytest.mark.parametrize('items_count, expected_li_count', [
    (1, 1),
    (3, 3),
    (5, 5),
    (6, 5),
    (11, 5)
])
def test_department_list_should_return_items(client, items_count, expected_li_count):
    create_departments(items_count)
    res = client.get(url_for('company_management.departments_list'))
    assert res.status_code == 200
    assert b'No departments' not in res.data
    for i in range(expected_li_count):
        assert f'Dep {i}'.encode('utf-8') in res.data
    for i in range(expected_li_count, items_count):
        assert f'Dep {i}'.encode('utf-8') not in res.data


@pytest.mark.parametrize('items_count, page, first_idx, last_idx', [
    (1, 1, 0, 0),
    (3, 1, 0, 3),
    (5, 1, 0, 5),
    (6, 1, 0, 4),
    (6, 2, 5, 5),
    (11, 2, 5, 9),
    (11, 3, 10, 10)
])
def test_paginated_department_list_should_return_items(client, items_count, page, first_idx, last_idx):
    create_departments(items_count)
    res = client.get(url_for('company_management.departments_list', page=page))
    assert res.status_code == 200
    assert b'No departments' not in res.data
    for i in range(items_count):
        if first_idx <= i <= last_idx:
            assert f'<li>Dep {i}</li>'.encode('utf-8') in res.data
        else:
            assert f'<li>Dep {i}</li>'.encode('utf-8') not in res.data


@pytest.mark.parametrize('items_count, page', [
    (1, 0),
    (3, 2),
    (5, 3),
    (9, 3),
    (10, 4)
])
def test_department_list_wrong_page_should_return_404(client, items_count, page):
    create_departments(items_count)
    res = client.get(url_for('company_management.departments_list', page=page))
    assert res.status_code == 404
    assert b'No departments' not in res.data
