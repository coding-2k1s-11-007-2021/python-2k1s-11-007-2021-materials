from flask import url_for


def test_main_page(client):
    res = client.get(url_for('company_management.main_page'))
    assert res.status_code == 200
    assert b'Main page content' in res.data
