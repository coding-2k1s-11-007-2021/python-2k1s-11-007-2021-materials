import pytest
from flask import url_for
from app import db

from company_management.models import Department


@pytest.mark.parametrize('name', [
    'IT',
    '1',
    '1' * 64
])
def test_create_department_should_redirect(client, name):
    res = client.post(url_for('company_management.create_department_page'), data=dict(name=name), follow_redirects=True)
    assert res == 200
    assert res.request.path == url_for('company_management.departments_list')


@pytest.mark.parametrize('name', [
    'IT',
    '1',
    '1' * 64
])
def test_create_department_should_create_new_department(client, name):
    client.post(url_for('company_management.create_department_page'), data=dict(name=name), follow_redirects=True)
    assert Department.query.count() == 1
    assert Department.query.filter_by(name=name).count() == 1


@pytest.mark.parametrize('name', [
    '',
    '1' * 65
])
def test_create_invalid_department_should_not_create_new_department(client, name):
    client.post(url_for('company_management.create_department_page'), data=dict(name=name), follow_redirects=True)
    assert Department.query.count() == 0


@pytest.mark.parametrize('name', [
    '',
    '1' * 65
])
def test_create_invalid_department_should_not_redirect(client, name):
    res = client.post(url_for('company_management.create_department_page'), data=dict(name=name), follow_redirects=True)
    assert res == 200
    assert res.request.path == url_for('company_management.create_department_page')


def test_create_empty_department_should_not_redirect(client):
    res = client.post(url_for('company_management.create_department_page'), follow_redirects=True)
    assert res == 200
    assert res.request.path == url_for('company_management.create_department_page')


def test_create_empty_department_should_not_create_new_department(client):
    client.post(url_for('company_management.create_department_page'), follow_redirects=True)
    assert Department.query.count() == 0


def test_create_not_unique_department_should_not_create_new_department(client):
    db.session.add(Department(name='IT'))
    db.session.commit()
    client.post(url_for('company_management.create_department_page'), data=dict(name='IT'), follow_redirects=True)
    assert Department.query.count() == 1


def test_create_not_unique_department_should_not_redirect(client):
    db.session.add(Department(name='IT'))
    db.session.commit()
    res = client.post(url_for('company_management.create_department_page'), data=dict(name='IT'), follow_redirects=True)
    assert res == 200
    assert res.request.path == url_for('company_management.create_department_page')
    assert b'Name of department should be unique' in res.data
