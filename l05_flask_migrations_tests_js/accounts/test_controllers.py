from flask import url_for


def test_current_user_info_for_logged_in_should_be_ok(logged_in_client):
    res = logged_in_client.get(url_for('accounts.current_user_info'))
    assert res.status_code == 200
    assert b'Rustem' in res.data


def test_current_user_info_for_logged_out_should_be_ok(client):
    res = client.get(url_for('accounts.current_user_info'))
    assert res.status_code == 401
    assert b'Rustem' not in res.data
