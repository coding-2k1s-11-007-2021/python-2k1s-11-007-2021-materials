import socket
import threading


shared_dict = {}


# rw lock https://gist.github.com/tylerneylon/a7ff6017b7a1f9a506cf75aa23eacfd6


def handle_client(conn, addr, semaphore):
    with semaphore:
        print('Starting new thread for client:', addr)
        read_file = conn.makefile(mode='r', encoding='utf-8')
        write_file = conn.makefile(mode='w', encoding='utf-8')
        write_file.write('Hi client\n')
        write_file.flush()
        cmd = read_file.readline().strip()
        while cmd:
            cmd, *args = cmd.split()
            if cmd == 'get':
                write_file.write(shared_dict.get(args[0], 'No value') + '\n')
                write_file.flush()
            elif cmd == 'set':
                shared_dict[args[0]] = args[1]
            else:
                write_file.write('Invalid command\n')
                write_file.flush()
            cmd = read_file.readline().strip()
        conn.close()


def main():
    host = '0.0.0.0'
    port = 8080
    semaphore = threading.Semaphore(3)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # solution for "[Error 89] Address already in use". Use before bind()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host, port))
    s.listen(5)

    try:
        while True:
            with semaphore:
                conn, addr = s.accept()
                t = threading.Thread(target=handle_client, args=(conn, addr, semaphore))
                t.daemon = True
                t.start()
    finally:
        if s:
            s.close()


if __name__ == '__main__':
    main()
