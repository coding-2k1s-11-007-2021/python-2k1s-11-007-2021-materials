import os

from flask import Flask

from accounts.accounts import accounts_blueprint
from company_management.company_management import company_management_blueprint
from local_configs import Configuration


app = Flask(__name__)


def main():
    app.register_blueprint(accounts_blueprint, url_prefix='/accounts')
    app.register_blueprint(company_management_blueprint, url_prefix='/')
    if not os.getenv('IS_PRODUCTION', None):
        app.config.from_object(Configuration)
    print(app.url_map)
    app.run()


if __name__ == '__main__':
    main()
