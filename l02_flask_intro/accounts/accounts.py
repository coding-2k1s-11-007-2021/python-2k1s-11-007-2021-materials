from flask import request, session, redirect, url_for, flash, render_template, Blueprint
from accounts.services import find_by_username_and_password, IncorrectPasswordError, UserNotFoundError


accounts_blueprint = Blueprint('accounts', __name__, template_folder='templates')


@accounts_blueprint.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        try:
            user = find_by_username_and_password(username, password)
        except IncorrectPasswordError:
            flash('Incorrect password.')
        except UserNotFoundError:
            flash('Incorrect username.')
        except Exception:
            flash('Unknown error.')
        else:
            session.clear()
            session['username'] = user['username']
            return redirect(url_for('company_management.main_page'))
    return render_template('accounts/login.html')
