from accounts.database import users_lst


class UserNotFoundError(Exception):
    pass


class IncorrectPasswordError(Exception):
    pass


def find_by_username_and_password(username, password):
    for user in users_lst:
        if user['username'] == username:
            if user['password'] == password:
                return user
            else:
                raise IncorrectPasswordError()
    raise UserNotFoundError()
